let app = new Vue({
    el: "#app",
    data() {
        return {
            shortLink: null,
            model: {
                originUrl: null,
                _token: null
            },
            loading: false,
            error: null
        }
    },
    methods: {
        submit() {
            let me = this;
            this.loading = true;
            this.model['_token'] = document.getElementById('_token').value;
            axios.post('', this.model)
                .then(function (response) {
                    console.log(response.data);
                    me.loading = false;
                    me.shortLink = response.data.shortLink
                })
                .catch(function (error) {
                    me.loading = false;
                    console.log(error);
                });
        }
    }
});