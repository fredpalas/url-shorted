/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */


// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// const $ = require('jquery');

import Vue from 'vue';
// any CSS you require will output into a single css file (app.css in this case)
import '../sass/app.scss';
import '../sass/common.scss';
// import 'bootstrap/scss/bootstrap.scss';
// import 'bootstrap-vue/src/index.scss';
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue);

global.Vue = Vue;

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';