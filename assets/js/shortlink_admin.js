let app = new Vue({
    el: "#app",
    data: {
        url: '',
        currentPage: 1,
        isBusy: false,
        rows: 0,
        fields: [
            {
                key: 'id',
                sortable: true
            },
            {
                key: 'originUrl',
                sortable: true,
                label: 'Original URL'
            },
            {
                key: 'shortUri',
                sortable: true,
                label: 'Short Uri'
            },
            {
                key: 'clicks',
                sortable: true
            }
        ],
        sortBy: 'id',
        sortDesc: false,
        perPage: 10
    },
    methods: {
        myProvider(ctx) {
            this.isBusy = true;
            let url = this.url +
                '?page=' + ctx.currentPage +
                '&perPage=' + ctx.perPage +
                '&orderBy=' + ctx.sortBy +
                '&order=' + ((ctx.sortDesc) ? 'desc' : 'asc');
            let promise = axios.get(url);

            return promise.then(data => {
                this.rows = data.data.total;
                return data.data.items;
            }).catch(error => {
                console.log(error);
                this.rows = 0;
                return [];
            }).finally(() => {
                this.isBusy = false;
            });
        },
    }
});