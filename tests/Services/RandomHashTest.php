<?php


namespace App\Tests\Services;


use App\Services\RandomHash;
use PHPUnit\Framework\TestCase;

class RandomHashTest extends TestCase
{


    public function testGenerate()
    {
        $randomHash = new RandomHash();

        $hash = $randomHash->generate(10);

        $this->assertTrue(strlen($hash) == 10);
        $this->assertIsString($hash);
    }

}