<?php


namespace App\Tests\Services;


use App\Entity\ShortLink;
use App\Repository\ShortLinkRepository;
use App\Services\Paginator;
use App\Services\RandomHash;
use App\Services\ShortLinkService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator as PaginatorClass;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Serializer;

class ShortLinkServiceTest extends TestCase
{

    /** @var EntityManagerInterface | MockObject */
    private $entityManager;
    /** @var RequestStack | MockObject */
    private $requestStack;
    /** @var Serializer | MockObject */
    private $serializer;
    /** @var Paginator | MockObject */
    private $paginator;
    /** @var RandomHash | MockObject */
    private $randomHash;
    /** @var ShortLinkService */
    private $shortLinkService;
    /** @var ShortLinkRepository | MockObject */
    private $shortLinkRepository;
    /** @var MockObject | Request */
    private $request;

    const ANY_URL = 'ANY_URL';
    const ANY_HASH = 'ANY_HASH';
    const ANY_DOMAIN_AND_HOST = 'http://domain.any';

    public function setUp()
    {
        $this->entityManager = $this->getMockBuilder(EntityManagerInterface::class)->disableOriginalConstructor()->getMock();
        $this->requestStack = $this->getMockBuilder(RequestStack::class)->disableOriginalConstructor()->getMock();
        $this->serializer = $this->getMockBuilder(Serializer::class)->disableOriginalConstructor()->getMock();
        $this->paginator = $this->getMockBuilder(Paginator::class)->disableOriginalConstructor()->getMock();
        $this->randomHash = $this->getMockBuilder(RandomHash::class)->disableOriginalConstructor()->getMock();
        $this->shortLinkRepository = $this->getMockBuilder(ShortLinkRepository::class)->disableOriginalConstructor()->getMock();
        $this->request = $this->getMockBuilder(Request::class)->disableOriginalConstructor()->getMock();
        $this->shortLinkService = new ShortLinkService(
            $this->entityManager,
            $this->requestStack,
            $this->serializer,
            $this->paginator,
            $this->randomHash
        );
    }

    /**
     * @throws \Exception
     */
    public function testNewShortNewUrl()
    {
        $shortLink = new ShortLink();
        $shortLink->setOriginUrl(self::ANY_URL);
        $shortLink->setShortUri(self::ANY_HASH);
        $this->entityManager->method('getRepository')->with(ShortLink::class)->willReturn($this->shortLinkRepository);
        $this->shortLinkRepository->method('findOneBy')->with(['originUrl' => self::ANY_URL])->willReturn(null);
        $this->randomHash->method('generate')->with(ShortLinkService::HASH_LENGTH)->willReturn(self::ANY_HASH);

        $this->entityManager->expects(self::once())->method('persist')->with($shortLink);
        $this->entityManager->expects(self::once())->method('flush');

        $this->requestStack->expects(self::once())->method('getCurrentRequest')->willReturn($this->request);
        $this->request->expects(self::once())->method('getSchemeAndHttpHost')->willReturn(self::ANY_DOMAIN_AND_HOST);

        $url = $this->shortLinkService->shortNewUrl($shortLink);

        $this->assertIsString($url);
        $this->assertEquals(self::ANY_DOMAIN_AND_HOST.'/'.self::ANY_HASH, $url);
    }

    public function testShortNewUrl()
    {
        $shortLink = new ShortLink();
        $shortLink->setOriginUrl(self::ANY_URL);
        $shortLink->setShortUri(self::ANY_HASH);
        $this->entityManager->method('getRepository')->with(ShortLink::class)->willReturn($this->shortLinkRepository);
        $this->shortLinkRepository->method('findOneBy')->with(['originUrl' => self::ANY_URL])->willReturn($shortLink);

        $this->requestStack->expects(self::once())->method('getCurrentRequest')->willReturn($this->request);
        $this->request->expects(self::once())->method('getSchemeAndHttpHost')->willReturn(self::ANY_DOMAIN_AND_HOST);

        $url = $this->shortLinkService->shortNewUrl($shortLink);

        $this->assertIsString($url);
        $this->assertEquals(self::ANY_DOMAIN_AND_HOST.'/'.self::ANY_HASH, $url);
    }

    /**
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function testRedirectResponseUri()
    {
        $shortLink = new ShortLink();
        $shortLink->setOriginUrl(self::ANY_URL);
        $shortLink->setShortUri(self::ANY_HASH);

        $clicks = $shortLink->getClicks();

        $this->entityManager->method('getRepository')->with(ShortLink::class)->willReturn($this->shortLinkRepository);
        $this->shortLinkRepository->method('findOneBy')->with(['shortUri' => self::ANY_HASH])->willReturn($shortLink);

        $this->entityManager->expects(self::once())->method('persist')->with($shortLink);
        $this->entityManager->expects(self::once())->method('flush');

        $response = $this->shortLinkService->redirectResponseUri(self::ANY_HASH);

        $this->assertEquals($clicks + 1, $shortLink->getClicks());
        $this->assertInstanceOf(RedirectResponse::Class, $response);
        $this->assertEquals($response->getTargetUrl(), self::ANY_URL);
    }

    public function testGetLinksPaginate()
    {
        $now = new \DateTime();
        $shortLink = new ShortLink();
        $shortLink->setOriginUrl(self::ANY_URL);
        $shortLink->setShortUri(self::ANY_HASH);
        $shortLink->setCreatedAt($now);
        $shortLink->setUpdatedAt($now);
        $data = [
            'id' => null,
            'originUrl' => self::ANY_URL,
            'shortUri' => self::ANY_HASH,
            'created_at' => $now->format('Y-m-d\TH:i:sP'),
            'updated_at' => $now->format('Y-m-d\TH:i:sP'),
        ];
        /** @var QueryBuilder | MockObject $queryBuilder */
        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)->disableOriginalConstructor()->getMock();

        $this->entityManager->method('getRepository')->with(ShortLink::class)->willReturn($this->shortLinkRepository);
        $this->shortLinkRepository->method('createQueryBuilder')->with('shortLink')->willReturn($queryBuilder);
        $this->requestStack->expects(self::once())->method('getCurrentRequest')->willReturn($this->request);

        $this->request->expects(self::at(0))->method('get')->with('page')->willReturn(1);
        $this->request->expects(self::at(1))->method('get')->with('limit')->willReturn(10);
        $this->request->expects(self::at(2))->method('get')->with('orderBy', 'id')->willReturn('id');
        $this->request->expects(self::at(3))->method('get')->with('order', 'asc')->willReturn('asc');
        $paginator = $this->getMockBuilder(PaginatorClass::class)->disableOriginalConstructor()->getMock();
        $queryBuilder->method('orderBy')->with('shortLink.'.'id', 'asc');
        $this->paginator->method('paginate')->with($queryBuilder, 1, 10)->willReturn($paginator);
        $arrayIterator = $this->getMockBuilder(\ArrayIterator::class)->disableOriginalConstructor()->getMock();
        $paginator->method('getIterator')->willReturn($arrayIterator);
        $arrayIterator->method('getArrayCopy')->willReturn([$shortLink]);

        $this->serializer->method('normalize')->with([$shortLink])->willReturn(
            [
                $data,
            ]
        );

        $this->shortLinkRepository->method('getCount')->willReturn(1);

        $jsonResponse = $this->shortLinkService->getLinksPaginate();

        $this->assertInstanceOf(JsonResponse::class, $jsonResponse);
        $this->assertEquals($jsonResponse->getContent(), json_encode(['items' => [$data], 'total' => 1]));
    }

}