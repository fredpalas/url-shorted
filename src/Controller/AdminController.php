<?php


namespace App\Controller;


use App\Form\ShortLinkFormType;
use App\Services\ShortLinkService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class AdminController
 * @package App\Controller
 * @Route("admin")
 */
class AdminController extends AbstractController
{


    /**
     * @Route("/", name="admin_home_xhr", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @param ShortLinkService $shortLinkService
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getIndexXhr(ShortLinkService $shortLinkService)
    {

        return $shortLinkService->getLinksPaginate();
    }

    /**
     * @Route("/", name="admin_home", methods={"GET"})
     */
    public function index()
    {

        return $this->render(
            'admin/index.html.twig'
        );
    }


}