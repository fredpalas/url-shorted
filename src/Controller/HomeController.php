<?php


namespace App\Controller;


use App\Entity\ShortLink;
use App\Form\ShortLinkFormType;
use App\Services\ShortLinkService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private $formFactory;
    private $shortLinkService;

    public function __construct(FormFactoryInterface $formFactory, ShortLinkService $shortLinkService)
    {
        $this->formFactory = $formFactory;
        $this->shortLinkService = $shortLinkService;
    }


    /**
     * @Route("/", name="xhr_home", methods={"POST"}, condition="request.isXmlHttpRequest()")
     * @param Request $request
     * @return JsonResponse
     */
    public function xhrIndex(Request $request)
    {
        $shortLink = new ShortLink();

        $form = $this->createForm(ShortLinkFormType::class, $shortLink);

        $form->handleRequest($request);

        if (($form->isSubmitted() && !$form->isValid()) || !$form->isSubmitted()) {
            return new JsonResponse(['errors' => $this->getErrorsFromForm($form)]);
        }

        $url = $this->shortLinkService->shortNewUrl($shortLink);

        $msg = [
            "shortLink" => $url,
            "message" => "Url shorted correctly",
        ];

        return new JsonResponse($msg);
    }

    /**
     * @Route("/", name="home", methods={"GET"})
     */
    public function index()
    {
        $form = $this->formFactory->create(ShortLinkFormType::class);

        return $this->render(
            'home/index.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{uri}", name="get_uri", methods={"GET"})
     * @param $uri
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function getUri($uri)
    {
        return $this->shortLinkService->redirectResponseUri($uri);
    }


    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }
}