<?php

namespace App\Entity;



use Gedmo\Mapping\Annotation\SoftDeleteable;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
/**
 * @ORM\Entity(repositoryClass="App\Repository\ShortLinkRepository")
 * @SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ShortLink
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2000, unique=true )
     * @Assert\Url()
     */
    private $originUrl;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $shortUri;

    /**
     * @ORM\Column(type="integer")
     * @Assert\GreaterThanOrEqual(value="0")
     */
    private $clicks = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOriginUrl(): ?string
    {
        return $this->originUrl;
    }

    public function setOriginUrl(string $originUrl): self
    {
        $this->originUrl = $originUrl;

        return $this;
    }

    public function getShortUri(): ?string
    {
        return $this->shortUri;
    }

    public function setShortUri(string $shortUri): self
    {
        $this->shortUri = $shortUri;

        return $this;
    }

    public function getClicks(): ?int
    {
        return $this->clicks;
    }

    public function setClicks(int $clicks): self
    {
        $this->clicks = $clicks;

        return $this;
    }

    public function addClick()
    {
        $this->clicks++;
    }
}
