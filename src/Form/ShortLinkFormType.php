<?php

namespace App\Form;

use App\Entity\ShortLink;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShortLinkFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('originUrl', UrlType::class, [
                'required' => true,
                'attr' => [
                    'v-model'=> "model.originUrl",
                    "data-vv-as"=>"originUrl"
                ]
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ShortLink::class,
            'label' => 'Link to short'
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
