<?php


namespace App\Services;


class RandomHash
{
    /**
     * @param $hashLength
     * @return string
     * @throws \Exception
     */
    public function generate($hashLength)
    {
        $charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $str = '';
        $length = strlen($charset);

        while ($hashLength--) {
            $str .= $charset[random_int(0, $length - 1)];
        }

        return $str;
    }
}