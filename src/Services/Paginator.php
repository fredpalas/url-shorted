<?php


namespace App\Services;

use Doctrine\ORM\Tools\Pagination\Paginator as PaginatorClass;

class Paginator
{

    /**
     * @param $dql
     * @param int $page
     * @param int $limit
     * @return PaginatorClass
     */
    public function paginate($dql, $page = 1, $limit = 10)
    {
        $paginator = new PaginatorClass($dql);
        $paginator->setUseOutputWalkers(false);

        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) // Offset
            ->setMaxResults($limit); // Limit

        return $paginator;
    }
}