<?php


namespace App\Services;


use App\Entity\ShortLink;
use App\Repository\ShortLinkRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class ShortLinkService
{
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var RequestStack */
    private $requestStack;
    /** @var Serializer */
    private $serializer;
    /** @var Paginator */
    private $paginator;
    /** @var RandomHash */
    private $randomHash;

    const HASH_LENGTH = 10;

    public function __construct(
        EntityManagerInterface $entityManager,
        RequestStack $requestStack,
        SerializerInterface $serializer,
        Paginator $paginator,
        RandomHash $randomHash
    ) {
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
        $this->serializer = $serializer;
        $this->paginator = $paginator;
        $this->randomHash = $randomHash;
    }

    /**
     * @param ShortLink $shortLink
     * @return string
     * @throws \Exception
     */
    public function shortNewUrl(ShortLink $shortLink)
    {
        $repository = $this->entityManager->getRepository(ShortLink::class);

        $shortLinkPrevious = $repository->findOneBy(['originUrl' => $shortLink->getOriginUrl()]);

        if (!$shortLinkPrevious) {
            $hash = $this->randomHash->generate(self::HASH_LENGTH);
            $shortLink->setShortUri($hash);
            $this->entityManager->persist($shortLink);
            $this->entityManager->flush();
        }

        return $this->getUrl($shortLinkPrevious ?? $shortLink);
    }

    /**
     * @param $uri
     * @return RedirectResponse
     * @throws EntityNotFoundException
     */
    public function redirectResponseUri($uri)
    {
        $repository = $this->entityManager->getRepository(ShortLink::class);

        $shortLink = $repository->findOneBy(['shortUri' => $uri]);

        if (!$shortLink) {
            throw new EntityNotFoundException();
        }

        $shortLink->addClick();
        $this->entityManager->persist($shortLink);
        $this->entityManager->flush();

        return new RedirectResponse($shortLink->getOriginUrl());
    }

    /**
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getLinksPaginate()
    {
        /** @var ShortLinkRepository $repository */
        $repository = $this->entityManager->getRepository(ShortLink::class);

        $query = $repository->createQueryBuilder('shortLink');
        $request = $this->getRequest();

        $page = $request->get('page');
        $perPage = $request->get('limit');
        $sortBy = $request->get('orderBy', 'id');
        $order = $request->get('order', 'asc');

        $query->orderBy('shortLink.'.$sortBy, $order);


        $data = $this->serializer->normalize($this->paginator->paginate($query, $page, $perPage)->getIterator()->getArrayCopy());

        return new JsonResponse(['items' => $data, 'total' => $repository->getCount()]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Request|null
     */
    private function getRequest()
    {
        return $this->requestStack->getCurrentRequest();
    }

    /**
     * @param ShortLink $shortLink
     * @return string
     */
    private function getUrl(ShortLink $shortLink)
    {
        $domain = null;

        if ($request = $this->getRequest()) {
            $domain = $request->getSchemeAndHttpHost();
        }

        return ($domain ? $domain.'/' : '').$shortLink->getShortUri();
    }
}